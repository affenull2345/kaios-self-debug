#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static void usage_exit(void)
{
	fprintf(stderr,
		"Usage: forwarder <port-listen> <uds-connect> <addr>\n");
	exit(1);
}

/* An app without root permissions can't read the token */
#define TOKEN_PATH "/data/local/service/debug-forwarder/.forwarder_token"
#define BUF_SIZE 512

struct client {
	int authenticated;
	int fd1, fd2;
	struct sockaddr_in sa1;
	struct sockaddr_un sa2;
	struct client *next;
	size_t off, off_back;
	int queued, queued_back;
	char buf[BUF_SIZE], buf_back[BUF_SIZE];
};

static int read_token(struct client *cl)
{
	FILE *token_file;
	char token[33];
	int res;
	if(cl->authenticated) return 1;
	res = read(cl->fd1, cl->buf + cl->off, BUF_SIZE - cl->off);
	if(res == 0){
		return -1;
	}
	else if(res < 0){
		perror("read");
	}
	else {
		cl->off += res;
	}

	if(cl->off >= sizeof(token)){
		int i;
		volatile int auth = 1; /* Don't optimize */

		cl->queued = cl->off - sizeof(token);
		if(cl->queued > 0) cl->off = sizeof(token);
		else cl->off = 0;

		token_file = fopen(TOKEN_PATH, "r");
		if(!token_file){
			perror(TOKEN_PATH);
			return -1;
		}

		if(NULL == fgets(token, sizeof(token), token_file)){
			perror("read " TOKEN_PATH);
			fclose(token_file);
			return -1;
		}
		fclose(token_file);

		for(i = 0; i < sizeof(token); i++){
			if(token[i] != cl->buf[i]){
				auth = 0;
			}
		}
		if(auth){
			fprintf(stderr, "[Client authenticated] %s:%d\n",
				inet_ntoa(cl->sa1.sin_addr),
				ntohs(cl->sa1.sin_port));
			return 1;
		}
		else {
			fprintf(stderr, "[Client wrong token] %s:%d\n",
				inet_ntoa(cl->sa1.sin_addr),
				ntohs(cl->sa1.sin_port));
		}
		return -1;
	}

	return 0;
}

static int create_fwd_socket(struct sockaddr_un *sa, const char *path)
{
	int fd;

	sa->sun_family = AF_UNIX;
	strncpy(sa->sun_path, path, sizeof(sa->sun_path));
	sa->sun_path[sizeof(sa->sun_path)-1] = '\0';

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(fd < 0){
		perror("socket");
		return -1;
	}
	fcntl(fd, F_SETFL, O_NONBLOCK);

	if(connect(fd, (struct sockaddr*)sa, sizeof(*sa)) < 0 &&
		errno != EINPROGRESS)
	{
		perror("connect");
		close(fd);
		return -1;
	}

	return fd;
}

static int mainloop(int lfd, const char *path)
{
	struct client **ptmp, *tmp, *clients = NULL;
	fd_set readfds, writefds;
	int errcount = 0; /* Count consecutive errors */

	while(1){
		int mfd = 1, res;
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		for(tmp = clients; tmp; tmp = tmp->next){
			FD_SET(tmp->fd1, &readfds);
			if(tmp->fd1 >= mfd) mfd = tmp->fd1 + 1;
			if(tmp->authenticated){
				FD_SET(tmp->fd2, &readfds);
				if(tmp->queued > 0)
					FD_SET(tmp->fd2, &writefds);
				if(tmp->queued_back > 0)
					FD_SET(tmp->fd1, &writefds);
				if(tmp->fd2 >= mfd) mfd = tmp->fd2 + 1;
			}
		}
		FD_SET(lfd, &readfds);
		if(lfd >= mfd) mfd = lfd + 1;

		res = select(mfd, &readfds, &writefds, NULL, NULL);
		if(res < 0 && errno != EINTR){
			perror("select");
			if(errcount++ > 5) return 3;
			continue;
		}
		else {
			errcount = 0;
		}
		if(res == 0) continue;

		if(FD_ISSET(lfd, &readfds)){
			struct client *last_client = NULL;
			socklen_t size;

			tmp = malloc(sizeof(struct client));
			tmp->next = NULL;
			tmp->queued = 0;
			tmp->queued_back = 0;
			tmp->off = 0;
			tmp->off_back = 0;
			tmp->authenticated = 0;
			size = sizeof(tmp->sa1);
			tmp->fd1 = accept(lfd,
				(struct sockaddr*)&tmp->sa1,
				&size);
			if(tmp->fd1 < 0){
				perror("accept");
				free(tmp);
				continue;
			}
			fcntl(tmp->fd1, F_SETFL, O_NONBLOCK);

			for(ptmp = &clients; *ptmp; ptmp = &((*ptmp)->next))
			{
				last_client = *ptmp;
			}

			if(last_client){
				last_client->next = tmp;
			}
			else if(!clients){
				clients = tmp;
			}
			last_client = tmp;

			fprintf(stderr, "[Client connected] %s:%d\n",
				inet_ntoa(tmp->sa1.sin_addr),
				ntohs(tmp->sa1.sin_port));
		}
		for(ptmp = &(clients); *ptmp; ptmp = &((*ptmp)->next)){
			tmp = *ptmp;
			if(!tmp->authenticated){
				if(FD_ISSET(tmp->fd1, &readfds)){
					res = read_token(tmp);
					if(res == 1){
						tmp->fd2 = create_fwd_socket(
							&tmp->sa2, path);
						if(tmp->fd2 > 0){
							tmp->authenticated = 1;
						}
					}
					else if(res == -1){
						close(tmp->fd1);
						fprintf(stderr,
						"[Client error] %s:%d\n",
						inet_ntoa(tmp->sa1.sin_addr),
						ntohs(tmp->sa1.sin_port));
						*ptmp = tmp->next;
						free(tmp);
						if(!*ptmp)
							break;
					}
				}

				continue;
			}
			if(FD_ISSET(tmp->fd2, &writefds) &&
				tmp->queued > 0)
			{
				res = write(tmp->fd2, tmp->buf + tmp->off,
					tmp->queued);
				if(res < 0){
					perror("write");
				}
				else {
					tmp->off += res;
					tmp->queued -= res;
					if(tmp->queued == 0)
						tmp->off = 0;
				}
			}
			if(FD_ISSET(tmp->fd1, &writefds) &&
				tmp->queued_back > 0)
			{
				res = write(tmp->fd1, tmp->buf_back +
					tmp->off_back, tmp->queued_back);
				if(res < 0){
					perror("write");
				}
				else {
					tmp->off_back += res;
					tmp->queued_back -= res;
					if(tmp->queued_back == 0)
						tmp->off_back = 0;
				}
			}
			if(FD_ISSET(tmp->fd1, &readfds) &&
				tmp->queued == 0)
			{
				tmp->queued = read(tmp->fd1, tmp->buf,
					BUF_SIZE);
				if(tmp->queued == 0){
					close(tmp->fd1);
					close(tmp->fd2);
					fprintf(stderr,
					"[Client disconnected] %s:%d\n",
					inet_ntoa(tmp->sa1.sin_addr),
					ntohs(tmp->sa1.sin_port));
					*ptmp = tmp->next;
					free(tmp);
					if(!*ptmp)
						break;
					else
						continue;
				}
				else if(tmp->queued < 0){
					perror("read");
				}
			}
			if(FD_ISSET(tmp->fd2, &readfds) &&
				tmp->queued_back == 0)
			{
				tmp->queued_back = read(tmp->fd2, tmp->buf_back,
					BUF_SIZE);
				if(tmp->queued_back == 0){
					close(tmp->fd1);
					close(tmp->fd2);
					fprintf(stderr,
					"[Server disconnected] for %s:%d!\n",
					inet_ntoa(tmp->sa1.sin_addr),
					ntohs(tmp->sa1.sin_port));
					*ptmp = tmp->next;
					free(tmp);
				}
				else if(tmp->queued_back < 0){
					perror("read");
				}
			}
			if(!*ptmp) break;
		}
	}
}

int main(int argc, char * const *argv)
{
	int sockopt = 1;
	int lfd;
	unsigned int port;
	char *err = NULL;
	struct sockaddr_in sa;

	/*== ARGUMENTS ==*/
	if(argc < 4){
		usage_exit();
	}

	port = strtoul(argv[1], &err, 10);
	if(err && *err){
		fprintf(stderr, "Invalid port: Error near '%c'\n", *err);
		usage_exit();
	}

	if(port <= 0 || port >= 65536){
		fprintf(stderr, "Invalid port: Must be between 0 and 65536\n");
		usage_exit();
	}

	if(0 == inet_aton(argv[3], &(sa.sin_addr))){
		fprintf(stderr, "Invalid IP address\n");
		usage_exit();
	}

	/*== INITIALIZE ==*/
	sa.sin_family = AF_INET;
	sa.sin_port = htons((uint16_t)port);
	lfd = socket(AF_INET, SOCK_STREAM, 0);
	if(lfd < 0){
		perror("socket");
		return 2;
	}
	setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt));
	if(bind(lfd, (struct sockaddr*)&sa, sizeof(sa)) < 0){
		perror("bind");
		return 2;
	}
	if(listen(lfd, 15) < 0){
		perror("listen");
		return 2;
	}

	return mainloop(lfd, argv[2]);
}
