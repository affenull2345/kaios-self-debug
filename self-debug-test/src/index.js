import Client from './client';
import logState from './state';

window.onerror = function(err, source, lineno, colno, obj){
	var backtrace = obj.stack || '[backtrace unavailable]';
	alert(
`An error occurred in the application.

Error information:
${source}:${lineno}:${colno}: ${err}

${backtrace}
`
	);
}

logState('Initializing...');

console = {
	log() {
		logState('[X] ' + Array.from(arguments).join(' '));
	},
	error() {
		logState('[E] ' + Array.from(arguments).join(' '));
	},
	warn() {
		logState('[W] ' + Array.from(arguments).join(' '));
	},
	info() {
		logState('[I] ' + Array.from(arguments).join(' '));
	}
};

var client = new Client();

client.start();
