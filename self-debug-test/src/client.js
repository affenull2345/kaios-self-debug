import FirefoxClient from 'firefox-client';
import logState from './state';
import JSZip from 'jszip';

const token_alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_.";
const token_file = '/data/local/service/debug-forwarder/.forwarder_token';

function newToken(){
	return new Promise((resolve, reject) => {
		var token = "";
		for(var i = 0; i < 32; i++){
			token += token_alphabet[
				Math.floor(Math.random() *
					token_alphabet.length)];
		}
		try {
			var cmd = navigator.
				engmodeExtension.startUniversalCommand(
				`printf '${token}' > '${token_file}'`, true);
			cmd.onsuccess = function(){
				resolve(token);
			}
			cmd.onerror = function(){
				alert('Token: ' + token);
				resolve(token);
			}
		}
		catch(e){
			alert('Token: ' + token);
			resolve(token);
		}
	});
}

function pickPackage(){
	return new Promise((resolve, reject) => {
		var picker = new MozActivity({name: 'pick'});
		picker.onsuccess = function(){
			resolve(this.result.blob);
		}
		picker.onerror = function(){
			reject(new Error('File picker denied'));
		}
	});
}

function readPackage(blob){
	logState('Unpacking ' + blob.name);
	return JSZip.loadAsync(blob).then(pkg => {
		var metadata =
			pkg.file('metadata.json').async('string');
		var nested_zip =
			pkg.file('application.zip').async('uint8array');
		return Promise.all([metadata, nested_zip]);
	}).then(list => {
		var metadata = JSON.parse(list[0]);
		if('manifestURL' in metadata){
			logState('Loaded package');
			var url = new URL(metadata.manifestURL);
			return Promise.resolve({
				metadata,
				host: url.host,
				zip: list[1]
			});
		}
		else {
			return Promise.reject(new Error('Missing manifestURL'));
		}
	});
}

export default class Client {
	constructor(){
		this.client = new FirefoxClient();
		logState('Initialized');
		this.client.on("error", function(e){
			alert('Client error: ' + e);
		});

		this.client.on("end", function(){
			alert('Connection closed');
			window.close();
		});
	}

	start() {
		logState('Connecting...');
		newToken().then(token => {
			this.client.connect('127.0.0.1', 6000, token,
				this.onConnect.bind(this));
		});
	}

	onConnect(){
		logState('Connected');
		this.client.getWebapps((err, webapps) => {
			this.webapps = webapps;
			this.listApps();
		});
	}

	listApps(){
		window.addEventListener('keydown', e => {
			if(e.key === 'Enter'){
				pickPackage().then(readPackage).then(pkg => {
					logState('Installing ' +
						pkg.metadata.manifestURL +
						'...');
					this.webapps.installPackaged(pkg.zip,
						pkg.host, e => {
							if(e) console.error(e);
							else logState(
								'Installed!');
						});
				}).catch(e => console.error(e));
			}
		});
		logState('Press ENTER to install an app');
		this.webapps.listRunningApps((err, apps) => {
			if(err) console.error(err);
			alert(apps.map(app => JSON.stringify(app)).join(';\n'));
		});
	}
}
