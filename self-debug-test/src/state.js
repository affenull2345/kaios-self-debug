export default function logState(stat, color){
	var el = document.createElement('div');
	if(color) el.style.color = color;
	el.textContent = stat;
	document.getElementById('statelog').appendChild(el);
	el.scrollIntoView();
}
