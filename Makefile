# Makefile for self-debug

### General configuration
OBJECTS = forwarder.o
EXECUTABLES = debug-forwarder

CFLAGS = -g -Wall -Wmissing-prototypes -Wmissing-declarations \
	-Wstrict-prototypes -Wpointer-arith -Wwrite-strings -Wcast-qual \
	-Wcast-align -Wbad-function-cast -Wformat-security -Wformat-nonliteral \
	-Wmissing-format-attribute -Werror -O2 -pedantic

DEPS_MK = .deps.mk

### Build system code

.PHONY: all
all: $(DEPS_MK)
	@$(MAKE) -C . all_no_deps

.PHONY: clean
clean:
	rm -f $(OBJECTS) $(EXECUTABLES)


sinclude $(DEPS_MK)

.PHONY: all_no_deps
all_no_deps: $(EXECUTABLES)

debug-forwarder: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

%.o: %.c
	$(CC) -fPIC $(CPPFLAGS) -o $@ -c $< $(CFLAGS)
.deps.mk::
	@printf '' > $@; set -e; for source in $(OBJECTS:.o=); do \
		temp=$$(dirname $$source)/.$$(basename $$source).dep ; \
		$(CC) -MM -MG -MT $$source.o -o $$temp $(CPPFLAGS) \
			$$source.c; \
		cat $$temp >> $@; \
	done

### App builder

MODE=dev
.PHONY: app
app:
	cd self-debug-test && npm install && npm run build:$(MODE)
