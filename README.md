# Install

Requires temporary root. Replace `/path/to/ndk` with the path to the unpacked
[KaiOS NDK](ftp://ftp.kaiostech.com/ndk/android-ndk-r20b-linux-x86_64.tar.bz2).

```sh
make CC=/path/to/ndk/toolchains/llvm/prebuilt/linux-x86_64/bin/armv7a-linux-androideabi23-clang
make app
adb push debug-forwarder /data/local/service/debug-forwarder/debug-forwarder
adb shell /data/local/service/debug-forwarder/debug-forwarder 6000 /data/local/debugger-socket 127.0.0.1
```

This starts a server on the device. You can now install self-debug-test/dist
using WebIDE and you should see a list of open apps' manifestURLs. Press Enter
to install a packaged app.

# License

```
Copyright (C) 2021 Affe Null
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
